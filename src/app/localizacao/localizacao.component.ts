import { Component, OnInit } from '@angular/core';
import { } from '@types/googlemaps';
import { ViewChild } from '@angular/core';
import { stat } from 'fs';


@Component({

  selector: 'localizacao',
  templateUrl: './localizacao.component.html',
  styleUrls: ['./localizacao.component.css'],
})
export class LocalizacaoComponent implements OnInit {
  
  @ViewChild('gmap') gmapElement: any;
  @ViewChild('gmap2') gmap2Element: any;
  map: google.maps.Map;
  directionsDisplay; 
  directionsService = new google.maps.DirectionsService();
  constructor() { }

  ngOnInit() 
  {
   this.directionsDisplay = new google.maps.DirectionsRenderer(); 

    var latlng = new google.maps.LatLng(-16.6903915,-49.3137428);
    this.ConfigurarMap1();
    this.ConfigurarMap2();
  
  this.directionsDisplay.setMap(this.map);  
  var marker = new google.maps.Marker({
    map:this.map,
    draggable:true,
    icon:'../assets/images/Logo.png'
  });

  marker.setPosition(latlng);

  if(navigator.geolocation)
  {
    navigator.geolocation.getCurrentPosition(function(position){
      marker.setPosition(new google.maps.LatLng(position.coords.latitude,position.coords.longitude))
    },
    function(error){
      
    });
  } else
  {
    
  }
  
  $(document).ready(function()
  {
    $('.component-localizacao').css('margin-top','11em');
    $('#map1').css('margin','20px');
    $('#map2').css('margin','20px');
  });
}

setMapType(mapTypeId: string) {
  this.map.setMapTypeId(mapTypeId)    
  }

  ConfigurarMap1()
  {
    var mapProp = {
      center: new google.maps.LatLng(-16.6903915,-49.3137428),
      zoom: 19,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    }

    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    this.map.setMapTypeId("roadmap");
  }

  ConfigurarMap2(): any {
    var mapProp = {
      center: new google.maps.LatLng(-16.7169414,-49.3071363),
      zoom: 19,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    }

    this.map = new google.maps.Map(this.gmap2Element.nativeElement, mapProp);
    this.map.setMapTypeId("roadmap");
  }
}
