export enum XTipoDeImagemComponent
{
    UniformesEscolares = 1,
    UniformesEmpresariais = 2,
    ProdutosBale = 3,
    ProdutosNatacao = 4,
    RoupasFitness = 5
}

export enum XDados
{ 
    Localhost,
    Servidor
}