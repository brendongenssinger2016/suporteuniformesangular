import { log } from 'util';
import { XTipoDeImagemComponent } from './../Enumeradores/XTipoDeImagemComponent';
import { HttpParams } from '@angular/common/http';
import { Http, Response, RequestOptions } from '@angular/http';  
import {Observable} from 'rxjs/Observable';
import { Injectable, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/catch';  
import 'rxjs/add/observable/throw';  
import { XConstantes } from '../Auxiliares/XConstantes';

@Injectable()
export class ItensRoupasService {

  
  // getImagensSelect()
  // {
  //   return this.http.get("http://localhost:5000/api/Itens/ImagemItensDestaques")
  //   .map
  //   (
  //     (response:Response)=>
  //     {
  //       console.log(response);
  //       return response.json();
  //     }
  //   ).catch(this.erroHandler);
  // }
  // erroHandler(error:Response){
  //   console.log(error);
  //   return Observable.throw(error);
  // }
constructor(private http: Http) { }

async obterCaminhoDasImagens(pTipoImagem:string,pNomeEmpresa:string)
  {
    let _params = new HttpParams().set("NomeComponent",pTipoImagem).set("NomeEmpresa",pNomeEmpresa)
    let requestOption = new RequestOptions({params:_params});
    let webApi = XConstantes.returnWebApi("XImages/GetComponent/"+pTipoImagem+"/"+pNomeEmpresa); 

    var result_ = await this.http.get(webApi,requestOption).toPromise().then(result =>
      {
        console.log('result ' + result);
        
        return result.json();
      },error=>
      {
        return Observable.throw(error);
      });
  }
}
