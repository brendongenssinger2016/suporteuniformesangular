import { TestBed, inject } from '@angular/core/testing';

import { ItensRoupasService } from './itens-roupas.service';

describe('ItensRoupasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItensRoupasService]
    });
  });

  it('should be created', inject([ItensRoupasService], (service: ItensRoupasService) => {
    expect(service).toBeTruthy();
  }));
});
