import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItensRoupasComponent } from './itens-roupas.component';

describe('ItensRoupasComponent', () => {
  let component: ItensRoupasComponent;
  let fixture: ComponentFixture<ItensRoupasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItensRoupasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItensRoupasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
