import { async } from '@angular/core/testing';
import { log } from 'util';
import { Jsonp } from '@angular/http';
import { Location, DOCUMENT } from '@angular/common';
import { XConstantes } from './../Auxiliares/XConstantes';
import { XTipoDeImagemComponent } from './../Enumeradores/XTipoDeImagemComponent';
import { Router, ActivatedRoute, NavigationExtras, ParamMap } from '@angular/router';
import { RoupasItens } from './../Model/RoupasItens';
import { AppService } from './../appWeb/app.service';
import { ItensRoupasService } from './itens-roupas.service';
import { Component, OnInit, Output, Input, OnDestroy, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { switchMap, map } from 'rxjs/operators';
import { reject } from 'q';
import * as $ from 'jquery';
import { JsonpInterceptor } from '@angular/common/http';



@Component({
  selector: 'modelSelect',
  templateUrl: './itens-roupas.component.html',
  styleUrls: ['./itens-roupas.component.css','./../appWeb/spinner.css']
})
export class ItensRoupasComponent implements OnInit{

  
listaDeRoupas : RoupasItens;
NomeAba : string;
@Output()
caminhoDasImagens : EventEmitter<Observable<string>> = new EventEmitter<Observable<string>>();
caminhoDasImagensManual : string[];
@Input() valor;
loading = false;
objects : string[];
servidorWebApi = XConstantes.retornaImage();
tipoImagem : string;
sobreTipoImage :string;
caminhoPadrao = "../assets/images/Products/";

  ngOnInit() 
  {
    //this.ObterCaminhoDasImagensAPI();
    this.ObterCaminhoDasImagensManual();
  }

  ObterCaminhoDasImagensManual(): any {
    

    this.tipoImagem =  this.route.snapshot.paramMap.get('id');
    console.log(this.tipoImagem=="UniformesEscolares");
    
    switch (this.tipoImagem) {
      case "UniformesEscolares":
      var descricaoImagem= 
      [
        { _tituloImagem:"Saia Feminina" },
        { _tituloImagem:"Uniforme da Prefeitura" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
      ]
      console.log(descricaoImagem);
      this.ConfiguraDadosDaImagem(descricaoImagem,5); 
      this.tipoImagem = "Uniformes Escolares"; 
        break;
      case "UniformesEmpresariais":
      var descricaoImagem= 
      [
        { _tituloImagem:"Saia Feminina" },
        { _tituloImagem:"Uniforme da Prefeitura" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
      ]
      console.log(descricaoImagem);
      this.ConfiguraDadosDaImagem(descricaoImagem,7);  
      this.tipoImagem = "Uniformes Empresariais";
      break;
      case "ProdutosBale":
      var descricaoImagem= 
      [
        { _tituloImagem:"Saia Feminina" },
        { _tituloImagem:"Uniforme da Prefeitura" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
      ]
      console.log(descricaoImagem);
      
      this.ConfiguraDadosDaImagem(descricaoImagem,7);  
      this.tipoImagem = "Balé";
      break;
      case "ProdutosNatacao":
      var descricaoImagem= 
      [
        { _tituloImagem:"Saia Feminina" },
        { _tituloImagem:"Uniforme da Prefeitura" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
      ]
      console.log(descricaoImagem);
      this.ConfiguraDadosDaImagem(descricaoImagem,3);  
      this.tipoImagem = "Natação";
      break;
      case "RoupasFitness":
      $(document).ready(function()
      {
        $('.imgDados').css('height','500px');
      });
      var descricaoImagem= 
      [
        { _tituloImagem:"Saia Feminina" },
        { _tituloImagem:"Uniforme da Prefeitura" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
        { _tituloImagem:"teste" },
      ]
      console.log(descricaoImagem);
      this.ConfiguraDadosDaImagem(descricaoImagem,6);  
      this.tipoImagem = "Roupas FitNess";
      break;
      default:
      alert("Nenhuma imagem encontrada");
      return this.router.navigate(['./app-home']);
        
    } 
  }

  ConfiguraDadosDaImagem(descricaoImagem: any, arg1: any): any {
    var object_ = [];
    for (let index = 0; index <= arg1; index++) 
    {
      var img = this.caminhoPadrao+this.tipoImagem+"/"+index+".JPG";
      var dado = {
        caminho : img,
        tituloImagem : descricaoImagem[index]._tituloImagem,
      };
      object_.push(dado);

      }
      
      this.caminhoDasImagensManual = object_;
  }

  ObterCaminhoDasImagensAPI()
  {
    this.loading = true;  
      this.tipoImagem =  this.route.snapshot.paramMap.get('id');

      this.service.obterCaminhoDasImagens(null,'Suporte Uniformes').then(result=>
      {
        console.log('acessando registro.');
        this.loading = false;
        if(this.caminhoDasImagens != null)
          this.caminhoDasImagens = result[this.tipoImagem];
      }).catch(reject=>
      {
        Observable.throw(reject);
        this.loading=false;
        $(document).ready(function(){
          $("#btnModal").click();
        });
      })  ;
  }



  constructor(
    private route : ActivatedRoute,
    private router:Router,
    private service: ItensRoupasService) 
  { 
   }
}
