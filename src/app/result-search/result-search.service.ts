import { Observable } from 'rxjs/Observable';
import { log } from 'util';
import { XConstantes } from './../Auxiliares/XConstantes';
import { Injectable } from '@angular/core';
import { Http, Jsonp } from '@angular/http';

@Injectable()
export class ResultSearchService {

  constructor(private http: Http) { }

  getListDados(dados:string)
  { 
    return XConstantes.SearchDados(dados);
    
  }
}
