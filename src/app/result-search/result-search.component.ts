import { ResultSearchService } from './result-search.service';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-result-search',
  templateUrl: './result-search.component.html',
  styleUrls: ['./result-search.component.css']
})
export class ResultSearchComponent implements OnInit {
  @Output()
  results : EventEmitter<Observable<string>> = new EventEmitter<Observable<string>>();
  constructor(private router:ActivatedRoute,
  private service:ResultSearchService) 
  {
    
   }

  ngOnInit() 
  {
    // let textMensagem = this.router.snapshot.paramMap.get('source');
    // console.log('text mensagem '+textMensagem);
    
    // this.service.getListDados(textMensagem).subscribe(pX=>
    //   {
    //     this.results = pX;
    //     $(document).ready(function(){
    //       $('#btnModalSearch').click();
    //     })
    //   },error=>
    //   {
    //     $(document).ready(function(){
    //       $('#btnModalSearch').click();
    //     })
    //   });
  }

}
