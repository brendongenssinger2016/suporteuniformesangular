import { TestBed, inject } from '@angular/core/testing';

import { ResultSearchService } from './result-search.service';

describe('ResultSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResultSearchService]
    });
  });

  it('should be created', inject([ResultSearchService], (service: ResultSearchService) => {
    expect(service).toBeTruthy();
  }));
});
