import { XConstantes } from './../Auxiliares/XConstantes';
import { PostImagesDto } from './../Model/PostImagesDto';

import { element } from 'protractor';
import { Response } from '@angular/http';
import { Dados } from './../Auxiliares/Dados';
import { Router, NavigationExtras } from '@angular/router';
import { Component, OnInit, SecurityContext, Input } from '@angular/core';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import { NgForm } from '@angular/forms';
import { ValueTransformer } from '@angular/compiler/src/util';
import { DasboardService } from './dasboard.service';
import { Observable } from 'rxjs/Observable';
import { ImagesLoad } from '../Model/ImagesLoad';

@Component({
  selector: 'app-dashboard',
  templateUrl: './../dashboard/dashboard.component.html',
  styleUrls: ['./../dashboard/dashboard.component.css','./../appWeb/spinner.css']
})
export class DashboardComponent implements OnInit {
  newData  = new FormData();

  tituloImage : string[] = [];
  descricaoImage : string[] = [];
  nome : string = Dados.Nome;
  nomeAba :string;
  mostrarCampo :boolean=false;
  preencherCampo : string="";
  nomeComponent : string;
  valorBase64 = [];
  tipoImagem : number;
  loading :boolean;
  mensagem : string;
  CanMensagemModal : boolean;
  cont = 1;
  imagesArray =[];
  CanSalvar = false;
  servidorWebApi = XConstantes.retornaImage();
  constructor
  (
    private router : Router, 
    private servico : DasboardService, 
    private sanitizer: DomSanitizer
  )
  {}

  ngOnInit() 
  {
    
  }

upload(event)
{
  let files  = event.target.files;
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
              var reader = new FileReader();

              reader.onload = (event) => {
                this.newData.append("file["+i+"]",files[i]);
                this.imagesArray.push((<FileReader>event.target).result); 

              }

              reader.readAsDataURL(event.target.files[i]);
      }
  }
}
salvarImagens():any
{
  this.loading = true;
  this.mensagem = "Fazendo upload das imagens, aguarde ..."; 
  console.log(this.tipoImagem);
  this.servico.uploadFile(this.newData,this.tipoImagem,this.tituloImage,this.descricaoImage)
  .subscribe(()=>
  {
    this.loading = false;
  },()=>
  {
    this.loading=false;
  },()=>
  {
    this.loading=false; 
  });
}

  erroHandler(error:Response) 
  {
    this.loading=false;
    
    this.loading = false;
    let dados : NavigationExtras = 
    {
      queryParams:
      {
        "mensagemDeErro":'Mensagem => '+error
      }
    }
    this.router.navigate(['./app-tela-de-erro'],dados)
    return Observable.throw(error);
  }

DeletarImagem(pObject : ImagesLoad)
{
  
  console.log(pObject);
  const _index = this.imagesArray.indexOf(pObject,1);
  if(_index >-1)
  {
    this.imagesArray.splice(_index,1);
  }
  else
  {
    this.imagesArray.splice(0,1);
  }
  
  
  
  //const fData :FormData = new FormData;
  //console.log("Quantidade "+event.target.files.length);
  // for (var i = 0; i < event.target.files.length; i++) 
  // {
    
  //     fData.append("file["+i+"]", event.target.files[i]);
  //     console.log("fdata Append" + event.target.files[i]);
  // }
  
}

DeletarImagemDoBd(pValor:any)
{
  console.log(pValor);
  console.log(pValor.caminhoImagem);
  this.loading=true;  
  this.imagesArray.splice(pValor.Chave_Itens_image,1);
  this.servico.deletarImageBD(pValor).subscribe(()=>
    {
      
    },
    this.erroHandler,
    ()=>{
      this.loading = false;
    });
    
}
  carregaComponentDashboard(pNomeComponent : string)
  {  
    this.nomeComponent = pNomeComponent;
    this.imagesArray = [];
    if(pNomeComponent =="PostarFotos")
    {
      this.loading = true;
      this.mensagem = "Carregando componentes, aguarde por favor ...";

      this.servico.getComponentDashboard()
      .subscribe
      (
        x=>
        {
          this.CanMensagemModal = true;
        },error=>
        {
          this.loading=false;
          let dados : NavigationExtras = 
          {
            queryParams:
            {
              "mensagemDeErro":error
            }
          }
          
          this.router.navigate(['app-tela-de-erro'],dados);
        },()=>
        {
          this.loading = false;
        }
      );

      this.loading=false;
    }
    
  }

  ClickEdit(pNome: string)
  {
    console.log("teste "+pNome);
    this.mostrarCampo = true;
    this.preencherCampo = pNome;
  }
  Cancelar()
  {
    this.mostrarCampo= false;
    this.preencherCampo = '';
  }

  OnTituloImage(event,pImage:any)
  {
    if(event.target.value=="")
      this.CanSalvar=false;
    else
      this.CanSalvar=true;
    var positionImagem = this.imagesArray.indexOf(pImage);
    console.log('Posicao image '+ positionImagem);
    this.tituloImage[positionImagem] = event.target.value;
    console.log(this.tituloImage);
  }

  OnDescricaoImage(event,pImage:any)
  {
    var positionImagem = this.imagesArray.indexOf(pImage);
    console.log('Posicao image '+ positionImagem);
    this.descricaoImage[positionImagem] = event.target.value;
    console.log(this.descricaoImage);
    
  }

  OnChange(event)
  {
    this.loading = true;
    console.log(event.target.value);
    
    var result = event.target.value;
    this.servico.getImages(result)
    .subscribe(x=>
      {
        this.imagesArray = x;
        console.log(x);
      },this.erroHandler,
      ()=>
      {
        this.loading=false;
      })
      
  }
}
