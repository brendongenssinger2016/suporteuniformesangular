import { AppComponent } from './../appWeb/app.component';
import { XDados, XTipoDeImagemComponent } from './../Enumeradores/XTipoDeImagemComponent';
import { XConstantes } from './../Auxiliares/XConstantes';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Http, Response, Headers, URLSearchParams, Jsonp, RequestOptions } from '@angular/http';  
import { Observable } from 'rxjs/Observable';
import { PARAMETERS } from '@angular/core/src/util/decorators';

@Injectable()
export class DasboardService {

  deletarImageBD(arg0: any) {
    
    console.log('result '+arg0.chave_Itens_image);
    console.log(XConstantes.returnWebApi("XImages/DeletarComponent"+"/"+arg0.chave_Itens_image));
    
    return this.http.delete(XConstantes.returnWebApi("XImages/DeletarComponent"+"/"+arg0.chave_Itens_image));
  }

  getImages(pTypeSelect:string)
  {
    return this.http.get(XConstantes.returnWebApi("XImages/BuscarImagens/"+pTypeSelect))
    .map
    (
      (resposta:Response)=>
      {
        return resposta; 
      }
    ).catch(this.erroHandler);
  }
  
  constructor(private http: HttpClient, private httpCliente:HttpClient) { }
  

  
  getComponentDashboard()
  {
    return this.http.get(XConstantes.returnWebApi("Dashboard/GetComponent"))
        .map
        (
          (response:Response)=>
          {
            return response;
          }
        )
        .catch(this.erroHandler);
  }

  erroHandler(error:Response){
    console.log(error);
    return Observable.throw(error);
  }

  uploadFile(data:FormData,Tipo : any,TituloImage:any,DescricaoImage:any) 
  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })};

    var empresa = XConstantes.SuporteUniformes;
    console.log("tipo Selecionado -> "+Tipo);
    
    data.append('Tipo',Tipo);
    data.append('Empresa',empresa);
    data.append('TitulosImages',TituloImage);
    data.append('DescricaoImage',DescricaoImage);

    return this.httpCliente.post(XConstantes.returnWebApi("XImages/SalvarImagens"),
    data
  );
  }
}
