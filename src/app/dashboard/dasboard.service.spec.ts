import { TestBed, inject } from '@angular/core/testing';

import { DasboardService } from './../dashboard/dasboard.service';

describe('DasboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DasboardService]
    });
  });

  it('should be created', inject([DasboardService], (service: DasboardService) => {
    expect(service).toBeTruthy();
  }));
});
