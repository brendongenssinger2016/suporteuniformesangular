import { Contatos } from './../Model/Contatos';
import { ContatosService } from './contatos.service';
import { Component, OnInit, NgModule } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-contatos',
  templateUrl: './contatos.component.html',
  styleUrls: ['./contatos.component.css']
})
export class ContatosComponent{

  constructor(private contatoService: ContatosService) { }

   Save(regForm: NgForm): void
{
  
  if(regForm == null)
    return;
  var contact = new Contatos;
  contact.Nome = regForm.value.Nome;
  contact.Sobrenome = regForm.value.SobreNome;
  contact.Endereco = regForm.value.Endereco;
  contact.Cep = regForm.value.Cep;
  contact.Cidade = regForm.value.Cidade;
  contact.Estado = regForm.value.Estado;
  contact.Mensagem = regForm.value.Mensagem;
  contact.Telefone=regForm.value.Telefone;
  this.contatoService.AddForm(contact)
  contact = null;
  regForm.reset();
}

}





