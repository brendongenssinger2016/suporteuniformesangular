import { XTipoDeImagemComponent } from './../Enumeradores/XTipoDeImagemComponent';
export class RoupasItens
{
    public imagemBase64 : string;
    public nomeDaRoupa : string;
    public descricaoRoupa :string;
    public tipoDeImagem : XTipoDeImagemComponent;
}