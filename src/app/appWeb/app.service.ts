import { Validators } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { XConstantes } from './../Auxiliares/XConstantes';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';  
import {Observable} from 'rxjs/Observable';
import { Injectable, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';  
import 'rxjs/add/operator/catch';  
import 'rxjs/add/observable/throw';  
import { Login } from './../Model/Login';
import { Token } from '@angular/compiler';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpResponse } from 'selenium-webdriver/http';
import { XTipoDeImagemComponent } from './../Enumeradores/XTipoDeImagemComponent';
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { resolve, reject } from 'q';


@Injectable()
export class AppService implements OnInit{
  
  
  
  constructor
  (
    private http: Http,
    private httpCliente:HttpClient,
    private router:Router
  )
  {
    
  }

  ngOnInit(): void 
  {
    
  }
  
getHtmlMenu(){
    
      let webApi = XConstantes.returnWebApi("Site/GetmenuPrincipal");
      return this.http.get(webApi)
      .map
      (
          (response:Response)=>
          { 
            return response.json();
          }
      ).catch(XConstantes.RetornaError);
    }
  
  // erroHandler(error:Response)
  // {
  //   let dados : NavigationExtras = 
  //   {
  //     queryParams:
  //     {
  //       "mensagemDeErro":error
  //     }
  //   }
  //   this.router.navigate(['./app-tela-de-erro'],dados)
  // }

  getVencimento()
    {
      let  dataDeVencimento =XConstantes.returnWebApi("Home/Datavencimento");
        var _result = this.http.get(dataDeVencimento)
        .map(
          res=> {
            return res;
          }
        ).catch(XConstantes.RetornaError)
  }
}

