import { Search } from './../Model/Search';
import { ResultSearchComponent } from './../result-search/result-search.component';
import { ResultSearchService } from './../result-search/result-search.service';
import { ItensRoupasService } from './../itens-roupas/itens-roupas.service';
import { Location } from '@angular/common';
import { log } from 'util';
import { XConstantes } from './../Auxiliares/XConstantes';
import { resolve } from 'url';
import { ImagesLoad } from './../Model/ImagesLoad';
import { HomeService } from './../home/Services/home.service';
import { XVersion } from './../Auxiliares/XVersion';
import { Observable } from 'rxjs/Observable';
import { Component, Inject, OnInit, NgModule, HostListener, OnChanges, Output } from '@angular/core';  
import { Http, Headers, Response} from '@angular/http';  
import { AppService } from './app.service';
import { Router, ActivatedRoute, Route, NavigationExtras, NavigationStart, NavigationEnd } from '@angular/router';  
import { NgForm } from '@angular/forms';
import * as $ from 'jquery';
import { EventEmitter } from '@angular/core';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl:'./app.component.html',
  styleUrls: ['./app.component.css'
  ,'./app.component.query.css'
  ,'./spinner.css'
  ,'app.component.mediaQuerys.css']
})

export class AppComponent implements OnInit {
  
  versao : String = XVersion.Nome + XVersion.Versao
  dataVencimento:Date;
  dataAtual: Date = new Date();
  private NomePesquisa : string="";
  public loading : boolean = false;
  title = 'Suporte Uniformes';
  mostrarMensagem : boolean;
  location :Location;
  carregou_ : boolean;
  textPesquisa :string;
  mensagem :string;
  results : Search[];
  constructor(
    public http: Http, 
    private router: Router, 
    public _appService: AppService,
    private activeRoute : ActivatedRoute, 
    private route: ActivatedRoute,
    public _itensImageService : ItensRoupasService,
    private serviceSearch : ResultSearchService)
  {
    this.eventosDeRotas();
  }
  
  ngOnInit(): void 
  {
    
  }
  eventosDeRotas()
  {
    $(document).ready(function()
    {
      $("#searchData").mouseover(function()
          {
            $(this).animate({width:'25em'});
          }).mouseout(function()
          {
            console.log('alert');
            $(this).animate({width:'10em'});

          });
          $("#resultData").css('visibility','visible');
    });
    
    this.router.events.subscribe(pX=>
    {
        $(document).ready(function()
        {
          TratarImgsHeader();   
          TratarFooter();
          
          $('#imgHeader').css('height','8em');
          $('.imgCarousel').css('width','100%');

          function TratarImgsHeader()
          {
            $('.imgOne').css('background','black');
            $('.imgOne').css('opacity','0.8');
            $('.imgOne').addClass("text-center");
          }

          function TratarFooter()
          {
            $('.component-footer').css('margin-top','20px');
            $('#btnModalSearch').hide();
            $('#btnError').hide();
            $('.component-footer').css("color","#FFF");
            // $('.component-footer,.container').css("margin-top","1em");
            $('.component-footer,p').css("margin-top","-10px");
            $("#corMenus,#menus").css("color","#FFF");
          }

          
          
        //   $.ajax({
        //     url: "/Employees/SearchEmployees",
        //     dataType: "json",
        //     data: {
        //         searchText: request.term
        //     },
        //     success: function (data) {
        //         response($.map(data.employees, function (item) {
        //             return {
        //                 label: item.name,
        //                 value: item.id
        //             };
        //         }));
        //     }
        // });
          
        });

        if(pX instanceof NavigationStart)
        {
          
        }
        if(pX instanceof NavigationEnd)
        {
          
         if(pX.urlAfterRedirects.indexOf('itemRoupas')>0)
         {
            if(XConstantes._reload)
            {
              location.reload();
            }
            console.log('Navigation end '+ (pX as NavigationStart).url);
            XConstantes._reload = true;
         }
        }
      });
  }

  EnviarParaComponent(pNomeComponent:string)
  {
    console.log('logg->' +this.route.component.toString());
    
    //this.router.navigate(['./app-itens-roupas/'+pNomeComponent]);
    this.router.navigateByUrl('./app-itens-roupas/'+pNomeComponent,{skipLocationChange:true}).then(()=>
    {
      this.router.navigate(['app-home'])
    });
  }
  
  erroHandler(error:Response)
  {
    console.log('true'+this.loading);
    this.loading = false;
    let dados : NavigationExtras = 
    {
      queryParams:
      {
        "mensagemDeErro":'Mensagem => '+error
      }
    }
    this.router.navigate(['./app-tela-de-erro'],dados)
  }

  PesquisarDados(regForm:NgForm)
  {
    this.mensagem = "realizando buscas, aguarde...";
    
    
    try
    {
      this.results = this.serviceSearch.getListDados(regForm.value.textPesquisa);
      
      if(this.results!=null)
      {
         $(document).ready(function()
         {
          //  $('#searchData').autocomplete({
          //    source:['Java','PHP']
          //  });
         });

      }

    }catch(errorHandler)
    {
      this.results = null;
    }

  }

  error(error)
  {
    console.error(error);
    
  }
}




