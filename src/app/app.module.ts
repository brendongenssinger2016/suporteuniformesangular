
import { LocalizacaoComponent } from './localizacao/localizacao.component';
import { ResultSearchService } from './result-search/result-search.service';
import { ItensRoupasService } from './itens-roupas/itens-roupas.service';
import { LoginService } from './login/login.service';
import { HomeService } from './home/Services/home.service';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppService } from './appWeb/app.service';
import { Http, HttpModule, Headers,RequestOptions } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component, AfterViewInit } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './appWeb/app.component';
import { ContatosComponent } from './contatos/contatos.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './home/home.component';

import { DicasComponent } from './dicas/dicas.component';
import { SobreLojaComponent } from './sobre-loja/sobre-loja.component';
import { ContatosService } from './contatos/contatos.service';
import { ItensRoupasComponent } from './itens-roupas/itens-roupas.component';
import { VencimentoComponent } from './vencimento/vencimento.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NeedAuthGuard } from './login/Guardian';
import { patch } from 'webdriver-js-extender';
import { DasboardService } from './dashboard/dasboard.service';
import { TelaDeErroComponent } from './tela-de-erro/tela-de-erro.component';
import { Location } from '@angular/common';
import { ResultSearchComponent } from './result-search/result-search.component';



const appRoute : Routes = 
[
  {path:'',component:HomeComponent},
  // {path:'' , redirectTo:'/app-home/app-itens-roupas' , pathMatch:'full'},
  {path:'app-tela-de-erro',component:TelaDeErroComponent},
  {path:'Home',component:HomeComponent},
  {
    path:'Home',component:HomeComponent,
    children:
    [ 
     
      {path:'app-vencimento',component:VencimentoComponent} 
    ],
  },
  {path:'localizacao',component:LocalizacaoComponent},
  {path:'result-search/:source',component:ResultSearchComponent},
  {path:'item/:id',redirectTo:'itemRoupas/:id'},
  {path:'itemRoupas/:id',component:ItensRoupasComponent},
  {path:'dashboard',component:DashboardComponent,
  children:
  [
   //{path:'menus-dashboard',component:MenusComponent}
  ],
  canActivate:[NeedAuthGuard]},

  {path:'login',component:LoginComponent},
  {path:'app-dicas',component:DicasComponent},
  
  {path:'app-sobre-loja',component:SobreLojaComponent},
  {path:'app-contatos',component:ContatosComponent},
  {path:'app-vencimento',component:VencimentoComponent} ,
  {path: 'app-login',component: LoginComponent},
  
  
];

const roupas : Routes = 
[
  
];

@NgModule({
  declarations: [
    AppComponent,
    ContatosComponent,
    HomeComponent,
    DicasComponent,
    SobreLojaComponent,
    ItensRoupasComponent,
    VencimentoComponent,
    LoginComponent,
    DashboardComponent,
    TelaDeErroComponent,
    ResultSearchComponent,
    LocalizacaoComponent

  ],
  imports: 
  [
    RouterModule.forRoot
    (
      appRoute,
      {preloadingStrategy: PreloadAllModules},
    ),
    RouterModule.forChild(roupas),
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    
    
  ],
  exports:[RouterModule],
  providers: 
  [
    AppService,
    ContatosService,
    DasboardService,
    HomeService,
    NeedAuthGuard,
    LoginService,
    ItensRoupasService,
    ResultSearchService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}