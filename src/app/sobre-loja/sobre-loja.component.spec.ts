import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SobreLojaComponent } from './sobre-loja.component';

describe('SobreLojaComponent', () => {
  let component: SobreLojaComponent;
  let fixture: ComponentFixture<SobreLojaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SobreLojaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SobreLojaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
