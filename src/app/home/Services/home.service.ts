import { resolve } from 'url';
import { Injectable } from '@angular/core';
import { XConstantes } from '../../Auxiliares/XConstantes';
import { Http, Response,Headers, URLSearchParams } from '@angular/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpResponse } from 'selenium-webdriver/http';
import { reject } from 'q';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HomeService {

  constructor(private http: Http) { }


getWidthImage()
{
  
  let webApi = XConstantes.returnWebApi("html/GetSizeImageCarousel"); 
    return this.http.get(webApi).map(
      res => 
      {
        return res; 
      },
    ).catch(XConstantes.RetornaError)    
}
}


