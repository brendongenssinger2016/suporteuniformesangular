import { HomeService } from './Services/home.service';
import{Component,OnInit} from '@angular/core';
import { Http, Headers, Response } from '@angular/http';  
import { AppService } from '../appWeb/app.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';  
import { AppComponent } from '../appWeb/app.component';

@Component({
  selector: 'Home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css','./homeMediaQuery.component.css','./../appWeb/spinner.css'],
  
})

export class HomeComponent implements OnInit{
  
  location : Location;
  componentAtual : string;
  loading : Boolean;

  ngOnInit(): void 
  {
    this.loading = false;
  }
  Rotas(name:string)
  {
    //this.router.navigate([name],{relativeTo:this.route});
  }
    
  constructor
  (
    public http: Http,
    private _route: Router, 
    private _appService:HomeService,
    private route: ActivatedRoute,
    private router : Router,
    private appComponent: AppComponent
  ) 
  {
    _route.events.subscribe(()=>
    {
      $(document).ready(function()
      {
        $('.imgHover').hover(function()
        {
          $(this).css('animation-delay','2s');
          $(this).css('opacity','0.7');
          $(this).css('box-shadow','5px 10px #dedada');
        },function()
        {
          $(this).css('opacity','1');
          $(this).css('box-shadow','');

        });
      });
    });
  }

  EnviarParaComponent(pValue:string)
  {
    console.log('acessour method');
    this.router.navigateByUrl('./app-itens-roupas/'+pValue,{skipLocationChange:true}).then(()=>
    {
      this.router.navigate(['app-home'])
    });


    this.componentAtual = pValue;
    
  }


}
