import { Search } from './../Model/Search';
import { Jsonp } from '@angular/http';
import { Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { XDados } from './../Enumeradores/XTipoDeImagemComponent';
import { forEach } from '@angular/router/src/utils/collection';
export class XConstantes
{

    static _localhost = "http://localhost:5000/api/";
    static _servidor  = "http://brendongenssin-001-site1.ftempurl.com/api/";
    static _imageLocal = "http://localhost:5000";
    static _imageServidor = "http://brendongenssin-001-site1.ftempurl.com/"
    static SuporteUniformes = "Suporte Uniformes"
//   apiLocalhost = "http://localhost:5000/api/";
//   apiServer = "http://brendongenssin-001-site1.ftempurl.com/api/";
    static componentUniformesEmpresarias;
    public static _reload = false;
    public static CaminhoImagens;


    static retornaImage()
    {
        return this._imageLocal;
    }
    
    static returnWebApi(pValor:string )
    {
        return XConstantes._localhost+pValor;
    }
    static TratarMensagemExcessao(pException:string)
    {
        if(pException.indexOf("with status: 0")!= -1)
            return "WebService desconectada,contate o administrador do sistema";
        else
            return "Não foi possível tratar a excessão";
               
    }

    static RetornaError(error:Response)
    {
        return Observable.throw(error);
    }



    static SearchDados(pSearch: string)
    {
        var novaLista = [];
        
        this.SEARCH.forEach(pX=>
            {
                pSearch.split(" ").forEach(element => 
                {
                    console.log("For each "+ element);
                    
                    var indexacaoEncontrada = pX.Palavras.find(
                            pC => pC == element);
                    console.log(indexacaoEncontrada)
                    if(indexacaoEncontrada != null)
                    {
                        console.log("indexacao diferente de nullo.")
                        var existList = novaLista.find(pR=> pR == pX);
                        if(existList == null)
                        {
                            novaLista.push(pX);
                            console.log(novaLista);
                        }

                    }
                });
            });
            
            console.log(JSON.stringify(novaLista));
            return novaLista;
    }

    static SEARCH: Search[] = 
    [   


        {
            Nome : "Uniformes Escolares",
            Caminho : "/item/UniformesEscolares",
            Palavras : 
            [
            
                "escolares","uniformes","uniforme","roupa","escolar","homens","menino",
                "short","calca","calça","calcas","calças","meninos","camisas","camisetas","garotos","garotas",
                "garota","garoto","meninas","meninos","rapazes","modelos","MODELOS",
            ]
        },
        {
            Nome : "Uniformes Empresariais",
            Caminho : "/item/UniformesEmpresariais",
            Palavras :
            [
                "empresas","empresarial","uniforme","roupa","empresa","cursos","cursinho",
                "curso","negocios","prefeituras","trabalho","governamental","privado","privada",
                "negócios","negócio","prefeito","homens","homem","menino","garoto","rapazes",
                "mulher","mulheres","meninas","camisas","camisetas","Empresariais","empresariais","empresa",
                "modelos","MODELOS"
            ]
        },

        {
            Nome : "Bale",
            Caminho : "/item/ProdutosBale",
            Palavras :
            [
                "bale","balé","bale","menina","mulher","danca","roupas","danca","dança","dançar",
                "dançarina","mulheres","meninas","uniformes","uniforme","dancarinas","modelos","MODELOS"
            ]
        },

        {
            Nome : "Natação",
            Caminho : "/item/ProdutosNatacao",
            Palavras : 
            [
                "agua","piscina","natacao","natacão","nadar","água","mergulho","rio","lago",
                "homens","mulheres","meninos","menino","garoto","garota","garotos","garotas","natação",
                "rapazes","rapaz","garotas","garota","modelos","MODELOS"
            ]
            
        },

        {
            Nome : "Roupas Fitness",
            Caminho : "/item/RoupasFitness",
            Palavras : 
            [
                "fitness","fit","fitnes","ftness","emagrecimento", "roupas",
                "academia","lycra","short","calca","cropped","tomara que caia",
                "mulher","mulheres","namoradas","meninas","menina","malhar",
                "academia","corrida","receitas","tomara","caia","body","elastano","garotas","modelos","MODELOS"
            ]

        }
        // {
        //     Nome : "Uniformes Escolares",
        //     Caminho : "/item/UniformesEscolares",
        //     Palavras :  
        //     [
        //         "Teste",
        //         "Teste1"
        //     ]
        // }
    ];
}