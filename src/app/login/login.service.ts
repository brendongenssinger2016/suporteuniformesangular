import { XConstantes } from './../Auxiliares/XConstantes';
import { Injectable } from '@angular/core';
import { Login } from '../Model/Login';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {

  constructor(private httpCliente:HttpClient) { }

  login(pLogin:Login) 
  {
    let htmlParams = new HttpParams();
    
    return this.httpCliente.post<any>(
      XConstantes.returnWebApi('login/GetLogin'),
      {user:pLogin.user,password:pLogin.password}
    );
      
  }

  getToken()
  {
    const TOKEN = 'TOKEN';
    return localStorage.getItem(TOKEN) != null;
  }

  setToken(token:string):void
  {
    const TOKEN = 'TOKEN';
    localStorage.setItem(TOKEN,JSON.stringify(token));
  }
  erroHandler(error:Response)
  {
      console.log(error.text);
      return Observable.throw(error);
  }
}
