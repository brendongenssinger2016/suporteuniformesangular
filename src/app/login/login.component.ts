import { LoginService } from './login.service';
import { Dados } from './../Auxiliares/Dados';
import { Token } from '@angular/compiler';


import { Login } from './../Model/Login';
import { Component, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Form, NgForm } from '@angular/forms';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  {
  login = new  Login();
   plogin:string;
   psenha:string;
   token:string;

  constructor(private api:LoginService, private router: Router) 
  { 
    var token = this.api.getToken();
    
    if(token)
      this.router.navigateByUrl('/dashboard');
  }
  
  tryLogin()
  {
    this.login.user = this.plogin;
    this.login.password = this.psenha;
    this.api.login(this.login).subscribe(c=> 
      {
        if(c.token)
        {
            Dados.Nome = c.nome;
            this.api.setToken(c.token);
            this.router.navigateByUrl('/dashboard',);
        }
      });
  }

}
