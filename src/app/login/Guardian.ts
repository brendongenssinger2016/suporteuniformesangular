import { Login } from './../Model/Login';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AppService } from '../appWeb/app.service';
import { LoginService } from './login.service';

@Injectable()
export class NeedAuthGuard implements CanActivate
{
    constructor(private customerService:LoginService,private router:Router){};
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const redirectUrl = route['_routerState']['url'];
        if(this.customerService.getToken())
        {
            return true;
        }
        this.router.navigateByUrl
        (
            this.router.createUrlTree
            (
                ['/login'],{queryParams:{redirectUrl}}
            )
        );
        return false;   
    }
    
    
}