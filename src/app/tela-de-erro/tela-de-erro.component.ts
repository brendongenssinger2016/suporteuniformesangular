import { XConstantes } from './../Auxiliares/XConstantes';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tela-de-erro',
  templateUrl: './tela-de-erro.component.html',
  styleUrls: ['./tela-de-erro.component.css']
})
export class TelaDeErroComponent implements OnInit {
  mensagemErro :string;
  canEnvioEmail : boolean;
  constructor(private route : ActivatedRoute, private router:Router) 
  {
    let _mensagemErro = this.route.queryParams.subscribe(params=>
      {
        this.mensagemErro = XConstantes.TratarMensagemExcessao(params["mensagemDeErro"]);
      });
      if(this.mensagemErro == null || this.mensagemErro == "")
        this.router.navigateByUrl('./app-home');
   }

  ngOnInit() {
  }
  habilitarEmail(){
    if(this.canEnvioEmail)
      this.canEnvioEmail = false;
  }

}
