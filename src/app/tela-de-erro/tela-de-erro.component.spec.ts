import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaDeErroComponent } from './tela-de-erro.component';

describe('TelaDeErroComponent', () => {
  let component: TelaDeErroComponent;
  let fixture: ComponentFixture<TelaDeErroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaDeErroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaDeErroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
